from flask import request
from flask_restful import Resource
from security.check_is_private import check_is_private
from utilities.request_check import request_check
from security.token import encode_token
from werkzeug.utils import secure_filename
from flask import send_file
# Todo: should be fixed later
from files_directory import file_directory

expected_request = ['token','target_user','image_address']

class SendPostFiles(Resource):

    def get(self):
        args = request.args
        if request_check(expected_request, args):
            return request_check(expected_request, args)

        current_user = encode_token(args['token'])
        target_user = args['user']

        if not check_is_private(current_user, target_user):
            return {'error' : True, 'message' : 'user is protected'}

        target_file = file_directory+'/'+secure_filename(args['target_user'])+'/'+secure_filename(args['image_address'])

        return send_file(target_file)
