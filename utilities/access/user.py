from mongo_config import db


def access_user(current_user, target_user):
    validate_user = db.user.find_one({'user': current_user})
    validate_target_user = db.user.find_one({'user': target_user})

    if not validate_user:
        return False
    if not validate_target_user:
        return False

    if current_user == target_user:
        return True

    private_mode_target_user = validate_target_user['private_mode']

    if not private_mode_target_user:
        return True

    validate_relation = db.user.find_one(
        {'user': current_user, 'following': {"$elemMatch": {"_id": validate_target_user['_id']}}})

    if validate_relation:
        return True

    return False

