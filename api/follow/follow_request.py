from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token
from utilities.request_check import check_post_request

send_follow_request_fields = ['user']

'''
follow target user if not following target user
and unfollow if following target user now
'''


class FollowRequest(Resource):
    # get user follow requests
    def get(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        user = encode_token(request.headers['token'])
        validation = db.user.find_one({'user': user}, {'_id': 0, 'follow_requests': 1})

        if validation is None:
            return {'error': True, 'message': 'token is wrong'}
        # create an array of follow requests id
        follow_request_ids = list(map(lambda follow_request_id: follow_request_id['_id'],
                                      list(validation['follow_requests'])))
        # find user and avatar from follow_requests_ids
        follow_request_users = db.user.find(
            {'_id': {'$in': follow_request_ids}},
            {'_id': 0, 'user': 1, 'avatar': 2})

        return {'error': False, 'follow_requests': list(follow_request_users)}

    # send follow request from one user to another user
    def post(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        if check_post_request(send_follow_request_fields, request.form):
            return check_post_request(send_follow_request_fields, request.form)

        current_user = encode_token(request.headers['token'])

        # check that current user and target user are not same
        if request.form['user'] == current_user:
            return {'error': True, 'message': 'origin user and target user can not be same'}

        # validate current user by his/her username
        validate_current_user = db.user.find_one({'user': current_user})

        if validate_current_user is None:
            return {'error': True, 'message': 'token is wrong'}

        # validate target user by his/her username
        validate_target_user = db.user.find_one({'user': request.form['user']})

        if validate_target_user is None:
            return {'error': True, 'message': 'target user is not valid'}

        # check that follow request has been sent already
        validate_following = db.user.find_one(
            {'user': request.form['user'],
             'follow_requests': {'$elemMatch': {'_id': validate_current_user['_id']}}})

        print(validate_following)
        if validate_following != None:
            db.user.update({'user': request.form['user']},
                           {'$pull': {'follow_requests': {'_id': validate_current_user['_id']}}})
            return {'error': False, 'message': 'follow requests is already rejected'}

        # un_follow if you are following target user
        validate_following = db.user.find_one(
            {'user': request.form['user'], 'followers': {'$elemMatch': {'_id': validate_current_user['_id']}}})
        if validate_following != None:
            db.user.update(
                {'user': request.form['user']},
                {'$pull': {'followers': {'_id': validate_current_user['_id']}}}
            )
            db.user.update({'user': current_user}, {'$pull': {'following': {'_id': validate_target_user['_id']}}})
            return {'error': False, 'message': 'you unfollowed target user successfully'}

        if validate_target_user['private_mode']:
            # push current user document id to follow request array
            db.user.update(
                {'user': request.form['user']},
                {'$push': {'follow_requests': {'_id': validate_current_user['_id']}}}
            )
            return {'error': False, 'message': 'follow request sent successfully'}

        else:
            # push current user document id to followers array
            db.user.update(
                {'user': request.form['user']},
                {'$push': {'followers': {'_id': validate_current_user['_id']}}}
            )
            # push target user document if to following array
            db.user.update({'user': current_user},
                           {'$push': {'following': {'user': {'_id': validate_target_user['_id']}}}})
            return {'error': False, 'message': 'follow request accepted successfully'}
