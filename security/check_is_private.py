from mongo_config import db


def check_is_private(current_user,target_user):
    target_user_find = db.find_one({'user': target_user})

    if not db.find_one({'user':current_user}) or not target_user_find:
        return False

    if target_user_find['private_mode']:
        if {'user':current_user} in target_user_find['followers']:
            return True
        return False
    return True
