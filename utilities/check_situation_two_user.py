from mongo_config import db


def check_two_user(current_user, target_user):
    target_user_all_data = db.user.find_one({'user': target_user})
    current_user_id = db.user.find_one({'user': current_user}, {'_id': 1})

    if {'_id': current_user_id['_id']} in target_user_all_data['follow_requests']:
        return 'requested'
    if {'_id': current_user_id['_id']} in target_user_all_data['followers']:
        return 'follower'

    return 'nothing'
