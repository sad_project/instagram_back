from mongo_config import db


# function get_db_variables returns
# variables needed to be read from database


def get_db_variables(current_user, target_user, form):

    user_validation = db.user.find_one({'user': current_user})
    target_user_validation = db.user.find_one({'user': target_user})
    target_post = None

    if target_user_validation:
        for i in target_user_validation['posts']:
            if i['post_id'] == (form['post_id']):
                target_post = i
    return user_validation, target_user_validation, target_post
