from flask import request
from flask_restful import Resource
import random

from mongo_config import db
from utilities.request_check import check_post_request
from utilities.return_users_id import return_users_id
from security.token import encode_token

expected_request = ['token', 'target_user']

# an api for starting a new chat with a new user


def generate_id():
    x = random.randint(1000000000000000000, 9999999999999999999)
    return str(x)


class StartChat(Resource):
    def post(self):
        if check_post_request(expected_request, request.form):
            return check_post_request(expected_request, request.form)

        current_user_name = encode_token(request.form['token'])
        target_user_name = request.form['target_user']

        users_document, users_id = return_users_id([current_user_name, target_user_name])

        if not users_id:
            return {'error': True, 'message': 'users not valid!'}

        current_user_id = users_id[0]
        target_user_id = users_id[1]
        current_user = users_document[0]

        for i in current_user['chats']:
            try:
                if i['target_user_id'] == target_user_id:
                    return {'error': False, 'message': 'chat with target user has been created before',
                            'chat_id': i['chat_id']}
            except KeyError:
                pass

        chat_id = generate_id()
        chat_document = {
            '_id': chat_id,
            'first_user_id': current_user_id,
            'second_user_id': target_user_id,
            'message': [],
        }
        db.chat.insert(chat_document)
        db.user.update({'_id': current_user_id}, {'$push': {'chats': {'target_user_id': target_user_id,
                                                                      'chat_id': chat_id}}})

        db.user.update({'_id': target_user_id}, {'$push': {'chats': {'target_user_id': current_user_id,
                                                                     'chat_id': chat_id}}})

        return {'error': False, 'message': 'chat created successfully', 'chat_id': chat_id}
