from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token


def check_following_posts_finished(following):
    check = True
    for user in following:
        if user['point'] != -1:
            check = False
    return check


def latest_post(following):
    # finding initial value for latest_index
    latest_index = 0
    for user in following:
        if user['point'] != -1:
            latest_index = following.index(user)
            break

    for user in following:
        if 'posts' in user and len(user['posts']) > 0 and user['point'] != -1 and int(
                user['posts'][user['point']]['post_id']) > int(
            following[latest_index]['posts'][following[latest_index]['point']]['post_id']):
            latest_index = following.index(user)
    return latest_index


class TimeLine(Resource):

    def get(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        username = encode_token(request.headers['token'])

        validation = db.user.find_one({'user': username}, {'following': 1, '_id': 2, 'posts': 3})
        if not validation:
            return {'error': True, 'message': 'token is wrong'}

        following = validation['following']
        following.append({'_id': validation['_id']})
        # we have following's id in his/her following list
        for id in following:
            following_user_id = id['_id']
            following_posts = db.user.find_one({'_id': following_user_id}, {'user': 1, 'posts': 2})
            id['posts'] = following_posts['posts']
            id['user'] = following_posts['user']
            # set initial point for user in order that his/her post be sorted with another users posts
            id['point'] = len(following_posts['posts']) - 1

        # selecting newest post among following list posts
        time_line_posts = []
        # loop over following's posts and finished when their posts push tp time_line_posts list sorted by time
        while not check_following_posts_finished(following):
            # fetch lasted following user
            latest_post_index = latest_post(following)
            # find user's latest post
            post = following[latest_post_index]['posts'][following[latest_post_index]['point']]
            # shift user's pointer(point) that the pointer points to the new latest post
            following[latest_post_index]['point'] -= 1
            # fetch user name from latest post
            post['user'] = following[latest_post_index]['user']
            # just send user and post id
            time_line_posts.append({'user': post['user'], 'post_id': post['post_id']})

        return {'error': False, 'posts': time_line_posts}
