from flask import request
from flask_restful import Resource

from mongo_config import db
from utilities.request_check import check_post_request
from utilities.return_users_id import return_users_id, return_user_name
from security.token import encode_token

expected_request = ['token', 'chat_id']


class GetMessages(Resource):
    def post(self):
        if check_post_request(expected_request, request.form):
            return check_post_request(expected_request, request.form)

        user_name = encode_token(request.form['token'])
        user_document, user_id = return_users_id([user_name])
        if not user_document:
            return {'error': True, 'message': "token error error",  'users': [], 'messages': []}

        chat = db.chat.find_one({'_id': request.form['chat_id']})

        if not chat:
            return {'error': True, 'message': "chat_id error",  'users': [], 'messages': []}

        user_names = return_user_name([chat['first_user_id'], chat['second_user_id']])

        if user_name not in user_names:
            return {'error': True, 'message': "access denied", 'users': [], 'messages': []}

        messages = []

        for i in chat['message']:
            if i['user_id'] == chat['first_user_id']:
                messages.append({'message': i['message'], 'user': user_names[0]})
            else:
                messages.append({'message': i['message'], 'user': user_names[0]})

        return {'error': False, 'message': "this is the chat messages", 'users': user_names, 'messages': messages}
