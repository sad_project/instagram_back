from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import get_token
from utilities.request_check import check_post_request

expected_request = ['user', 'password']


class LogIn(Resource):
    def post(self):
        if check_post_request(expected_request, request.form):
            return check_post_request(expected_request, request.form)

        user = request.form['user']
        password = request.form['password']

        validation = db.user.find_one({'user': user, 'password': password})

        if validation:
            return {'error': False, 'token': get_token(user), 'avatar': validation['avatar']}, 201, {
                'Access-Control-Allow-Origin': '*'}

        return {'error': True, 'message': 'username or password is wrong'}, 201, {'Access-Control-Allow-Origin': '*'}
