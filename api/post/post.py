import os

from flask import request
from flask_restful import Resource
from werkzeug.utils import secure_filename

from files_directory import file_directory, get_file_format
from mongo_config import db
from security.token import encode_token
from utilities.access.user import access_user
from utilities.hash_by_time import hash_by_time
from utilities.request_check import check_post_request
from utilities.request_check import request_check

add_new_post_fields = ['description']
remove_post_fields = ['post_id']
get_expected_request = ['post_id', 'user']


class Post(Resource):
    # add new post

    def post(self):

        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}
        # validate request
        if check_post_request(add_new_post_fields, request.form):
            return check_post_request(add_new_post_fields, request.form)
        # encode username from token
        username = encode_token(request.headers['token'])

        validation = db.user.find_one({'user': username})
        if not validation:
            return {'error': True, 'message': 'token is wrong'}
        user_path = file_directory + '/' + str(validation['_id'])
        # hash post by time
        hash_id = hash_by_time()
        file_name_array = []
        for file in request.files:
            file_name = hash_id + str(len(os.listdir(user_path)))
            filename = secure_filename(str(file_name) + '.' + get_file_format(request.files[file].filename))
            file_name_array.append(filename)
            request.files[file].save(os.path.join(user_path, str(filename)))

        db.user.update({'user': username},
                       {'$push': {'posts': {'post_id': hash_id, 'description': request.form['description'],
                                            'images': list(file_name_array), 'likes': [], 'comments': []}}})
        return {'error': False, 'message': "post created successfully"}

    def delete(self):
        args = request.args

        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        if request_check(remove_post_fields, args):
            return request_check(remove_post_fields, args)

        # encode username from token
        username = encode_token(request.headers['token'])

        validation = db.user.find_one({'user': username})

        if validation is None:
            return {'error': True, 'message': 'token is wrong'}
        # search on db by username and post id
        validate_post = db.user.find_one(
            {'user': username, 'posts': {'$elemMatch': {'post_id': args['post_id']}}})
        # if post does not exist response error
        if validate_post is None:
            return {'error': True, 'message': 'post not exist for this user'}
        # removable post
        post = validate_post['posts'][0]
        # removable images from user directory
        images = list(post['images'])

        user_path = file_directory + '/' + str(validation['_id'])
        # remove post from db
        db.user.update({'user': username},
                       {'$pull': {'posts': {'post_id': args['post_id']}}})
        # remove files from user directory
        for image in images:
            os.remove(user_path + '/' + image)

        return {'error': False, 'message': 'post removed successfully'}

    def get(self):

        args = request.args
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}
        if request_check(get_expected_request, args):
            return request_check(get_expected_request, args)

        current_user = encode_token(request.headers['token'])
        target_user = args['user']

        validate_user = db.user.find_one({'user': current_user})
        if not validate_user:
            return {'error': True, 'message': 'token is wrong'}

        validate_target_user = db.user.find_one({'user': target_user})

        if not validate_target_user:
            return {'error': True, 'message': 'target user is not valid'}

        if not access_user(current_user, target_user):
            return {'error': True, 'message': 'user does not have permission'}

        validate_post_query = db.user.find_one(
            {'user': target_user, 'posts': {'$elemMatch': {'post_id': args['post_id']}}}, {'posts.$': 1, 'avatar': 2})

        if not validate_post_query:
            return {'error': True, 'message': 'post id is not valid'}, 404

        validate_post = validate_post_query['posts'][0]
        # check if current user likes that specific post
        validate_post['liked'] = True if ({'_id': validate_user['_id']} in validate_post['likes']) else False

        # just return number of likes and comments
        validate_post['likes'] = len(validate_post['likes'])
        last_comments = []
        if validate_post['comments']:
            if len(validate_post['comments']) > 2:
                last_comments = [validate_post['comments'][-2], validate_post['comments'][-1]]
            else:
                last_comments = validate_post['comments']
        validate_post['comments'] = len(validate_post['comments'])
        # add target user name in object
        validate_post['user'] = target_user
        validate_post['avatar'] = validate_post_query['avatar']
        # set image url for each image
        validate_post["last_comments"] = last_comments
        for image in validate_post['images']:
            validate_post['images'][
                validate_post['images'].index(image)] = '/post/files?user=' + target_user + "&post_id=" + args[
                'post_id'] + '&file=' + image

        return validate_post

