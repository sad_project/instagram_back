from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token, get_token
from utilities.request_check import check_post_request

edit_profile_fields = ['user', 'password', 'name', 'last_name', 'private_mode']


class User(Resource):

    def get(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        username = encode_token(request.headers['token'])
        validation = db.user.find_one({'user': username},
                                      {'_id': 0, 'user': 1, 'password': 2, 'name': 3, 'last_name': 4,
                                       'private_mode': 5, 'avatar': 6, 'posts': 7,
                                       'followers': 8, 'following': 9})
        if not validation:
            return {'error': True, 'message': 'token is wrong'}
        # Todo: get length of array instead of fetch all of the array
        return {'error': False, 'personal_data': {'user': validation['user'], 'name': validation['name'],
                                                  'last_name': validation['last_name'],
                                                  'password': validation['password'],
                                                  'private_mode': validation['private_mode'],
                                                  'avatar': validation['avatar'],
                                                  'posts': len(validation['posts']),
                                                  'followers': len(validation['followers']),
                                                  'following': len(validation['following'])}}

    def put(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        form = request.form
        if check_post_request(edit_profile_fields, form):
            return check_post_request(edit_profile_fields, form)

        username = encode_token(request.headers['token'])
        validation = db.user.find_one({'user': username})

        # # validate token that user is exist
        if not validation:
            return {'error': True, 'message': 'token is wrong'}

        # check if the new username is not exist
        validation_new_user = db.user.find_one({'user': form['user']})
        if validation_new_user and username != validation_new_user['user']:
            return {'error': True, 'message': 'username is already in use'}

        # update user document
        db.user.update({'user': username},
                       {'$set': {'user': form['user'], 'password': form['password'], 'name': form['name'],
                                 'last_name': form['last_name'], 'private_mode': form['private_mode']}})
        return {'error': False, 'token': get_token(form['user'])}
