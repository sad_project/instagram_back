# function checks if the request.args
# matches the expected fields we need
# returns false otherwise returns the error


def request_check(expected_request, request_args):
    for field in expected_request:
        if field not in request_args:
            return {'error': True, 'message':'append ' + field}
    return False


def check_post_request(expected_request, request_body):
    for field in list(expected_request):
        if field not in list(request_body):
            return {'error': True, 'message': 'append ' + field + ' in request body'}, 201,\
                   {'Access-Control-Allow-Origin': '*'}
    return False
