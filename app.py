# users web services
from flask import Flask, send_from_directory
from flask_cors import CORS
from flask_restful import Api

from api.follow.follow_request import FollowRequest
from api.follow.followers import Follower
from api.follow.following import Following
# follow web services
from api.follow.handle_follow_request import HandleFollowRequest
# post web services
from api.post.comment_post import CommentPost
from api.post.get_post_file import GetPostFiles
from api.post.like_post import LikePost
from api.post.post import Post
from api.post.post_list import PostList
from api.post.time_line import TimeLine
# send file web services
from api.send_files.send_post_file import SendPostFiles
from api.user.avatar import ProfileAvatar
# user web services
from api.user.login import LogIn
from api.user.personal_data import User
from api.user.profile import Profile
from api.user.search import Search
from api.user.signup import SignUp

from chat_api.get_chat_list import GetChatList
from chat_api.get_messages import GetMessages
from chat_api.send_message import SendMessage
from chat_api.start_chat import StartChat

# web services
app = Flask(__name__, static_folder="./static")
CORS(app)
api = Api(app)


# serve web site
@app.route('/')
def root():
    return send_from_directory('static', 'index.html')


# user
api.add_resource(SignUp, '/user/signup', endpoint='signup')
api.add_resource(LogIn, '/user/login', endpoint='login')
api.add_resource(User, '/user/personal_data', endpoint='personal_data')
api.add_resource(Search, '/user/search', endpoint='search')
api.add_resource(Profile, '/user/profile', endpoint='profile')
api.add_resource(ProfileAvatar, '/user/avatar', endpoint='avatar')
# follow
api.add_resource(FollowRequest, '/follow_request', endpoint='follow_request')
api.add_resource(HandleFollowRequest, '/follow_request/handle', endpoint='handle_follow_request')
api.add_resource(Following, '/following', endpoint='following')
api.add_resource(Follower, '/follower', endpoint='follower')

# post
api.add_resource(Post, '/post', endpoint='post')
api.add_resource(GetPostFiles, '/post/files', endpoint='post_files')
api.add_resource(LikePost, '/post/like', endpoint='like_post')
api.add_resource(CommentPost, '/post/comment', endpoint='comment_post')
api.add_resource(TimeLine, '/post/time_line', endpoint='time_line')
api.add_resource(PostList, '/post/post_list', endpoint='post_list')

# send_files
api.add_resource(SendPostFiles, '/send_files/send_post_files', endpoint='send_post_files')

# chat
api.add_resource(StartChat, '/chat/start_chat', endpoint='start_chat')
api.add_resource(SendMessage, '/chat/send_message', endpoint='send_message')
api.add_resource(GetChatList, '/chat/get_chat_list', endpoint='get_chat_list')
api.add_resource(GetMessages, '/chat/get_messages', endpoint='get_messages')

if __name__ == '__main__':
    app.run()
