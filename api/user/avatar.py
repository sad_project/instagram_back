import os

from flask import request, send_file
from flask_restful import Resource

from files_directory import avatar_directory
from mongo_config import db
from security.token import encode_token
from utilities.hash_by_time import hash_by_time


class ProfileAvatar(Resource):

    def get(self):
        args = request.args
        if 'user' not in args:
            return {'error': True, 'message': 'append user'}
        # fetch user from coded user name
        user = args['user'].split('_')[0]
        validate_target_user = db.user.find_one({'user': user})
        if validate_target_user:
            file_path = avatar_directory + "/" + str(validate_target_user['_id']) + '.jpg'
            if os.path.isfile(file_path):
                return send_file(file_path)
            else:
                return send_file(avatar_directory + "/profile.jpg")
        else:
            return {'error': True, 'message': 'user is not valid'}

    def delete(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        username = encode_token(request.headers['token'])

        validate_user = db.user.find_one({'user': username})

        if validate_user:
            file_path = avatar_directory + "/" + str(validate_user['_id']) + ".jpg"
            # remove exsiting user avatar in other to override it
            if os.path.isfile(file_path):
                os.remove(file_path)

            avatar_link = '/user/avatar?user=' + username + '_' + hash_by_time()
            db.user.update({'user': username},
                           {'$set': {'avatar': avatar_link}})

            return {'error': True, 'message': 'avatar uploaded successfully', 'avatar': avatar_link}

        else:
            return {'error': True, 'message': 'token is wrong'}

    def post(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        username = encode_token(request.headers['token'])

        validate_user = db.user.find_one({'user': username})

        if validate_user:
            if 'avatar' not in request.files:
                return {'error': True, 'message': 'append avatar in body'}
            file_path = avatar_directory + "/" + str(validate_user['_id']) + ".jpg"
            # remove exsiting user avatar in other to override it
            if os.path.isfile(file_path):
                os.remove(file_path)

            request.files['avatar'].save(os.path.join(avatar_directory, str(validate_user['_id']) + ".jpg"))

            avatar_link = '/user/avatar?user=' + username + '_' + hash_by_time()
            db.user.update({'user': username},
                           {'$set': {'avatar': avatar_link}})

            return {'error': True, 'message': 'avatar uploaded successfully', 'avatar': avatar_link}
        else:
            return {'error': True, 'message': 'token is wrong'}
