from flask import request
from flask import send_file
from flask_restful import Resource

from files_directory import file_directory
from mongo_config import db
from utilities.request_check import request_check

get_post_files = ['user', 'post_id', 'file']


class GetPostFiles(Resource):

    def get(self):

        args = request.args
        if request_check(get_post_files, args):
            return request_check(get_post_files, args)

        target_user = args['user']

        validate_target_user = db.user.find_one({'user': target_user})

        if not validate_target_user:
            return {'error': True, 'message': 'target user is not valid'}

        validate_post = db.user.find_one(
            {'user': target_user, 'posts': {'$elemMatch': {'post_id': args['post_id']}}}, {'_id': 0, 'posts.$': 1})
        if not validate_post:
            return {'error': True, 'message': 'post id is not valid'}

        if args['file'] not in validate_post['posts'][0]['images']:
            return {'error': True, 'message': 'file is not valid'}

        file_path = file_directory + '/' + str(validate_target_user['_id']) + '/' + args['file']
        return send_file(file_path)
