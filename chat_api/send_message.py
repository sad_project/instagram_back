from flask import request
from flask_restful import Resource

from mongo_config import db
from utilities.request_check import check_post_request
from utilities.return_users_id import return_users_id
from security.token import encode_token

expected_request = ['token', 'chat_id', 'message']


class SendMessage(Resource):
    def post(self):
        if check_post_request(expected_request, request.form):
            return check_post_request(expected_request, request.form)

        user_name = encode_token(request.form['token'])
        user_document, user_id = return_users_id([user_name])
        if not user_document:
            return {'error': True, 'message': "token error error"}

        if not db.chat.find_one({'_id': request.form['chat_id']}):
            return {'error': True, 'message': "chat_id error"}

        db.chat.update({'_id': request.form['chat_id']},
                       {'$push': {'message': {'user_id': user_id, 'message': request.form['message']}}})

        return {'error': False, 'message': 'message sent successfully'}
