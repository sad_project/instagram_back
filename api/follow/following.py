from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token
from utilities.access.user import access_user
from utilities.check_situation_two_user import check_two_user
from utilities.request_check import request_check

get_following_fields = ['user']


class Following(Resource):

    def get(self):
        args = request.args
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        if request_check(get_following_fields, args):
            return request_check(get_following_fields, args)

        current_user = encode_token(request.headers['token'])
        target_user = args['user']

        validate_user = db.user.find_one({'user': current_user}, {'_id': 1})
        validate_target_user = db.user.find_one({'user': target_user}, {'_id': 0, 'following': 1})

        if not validate_user:
            return {'error': True, 'message': 'token is wrong'}

        if not validate_target_user:
            return {'error': True, 'message': 'target user is wrong'}

        if not access_user(current_user, target_user):
            return {'error': True, 'message': 'user does not have permission'}

        # create an array of following id
        following_ids = list(map(lambda follow_request_id: follow_request_id['_id'],
                                 list(validate_target_user['following'])))
        # find user and avatar from following_ids
        following_users = db.user.find(
            {'_id': {'$in': following_ids}},
            {'_id': 0, 'user': 1, 'avatar': 2})

        user_array = []
        for user in following_users:
            user['status'] = check_two_user(current_user, user['user'])
            user_array.append(user)

        return {'error': False, 'following': list(user_array)}

