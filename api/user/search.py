from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token
from utilities.check_situation_two_user import check_two_user
from utilities.request_check import request_check

expected_request = ['user']


class Search(Resource):

    def get(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        args = request.args
        if request_check(expected_request, args):
            return request_check(expected_request, args)

        username = encode_token(request.headers['token'])
        validation = db.user.find_one({'user': username})

        if validation is None:
            return {'error': True, 'message': 'token is wrong'}

        # Todo: set pagination and don't return all the users that their username have condition
        users = db.user.find({"user": {"$ne": username, "$regex": args['user'], "$options": "$i"}},
                             {"_id": 0, "user": 1, "avatar": 3})
        user_array = []
        for user in users:
            user['status'] = check_two_user(username, user['user'])
            user_array.append(user)

        return {'error': False, 'users': list(user_array)}
