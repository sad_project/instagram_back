# returns false if user doesn't exist
# returns id of users

from mongo_config import db


def return_users_id(users_list):
    document_list = []
    id_list = []
    for i in users_list:
        x = db.user.find_one({'user': i})
        if not x:
            return False, False
        document_list.append(x)
        id_list.append(x['_id'])
    return document_list, id_list


def return_user_name(id_list):
    user_list = []
    for i in id_list:
        x = db.user.find_one({'_id': i})
        if not x:
            return False, False
        user_list.append(x['user'])
    return user_list
