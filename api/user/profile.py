from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token
from utilities.check_situation_two_user import check_two_user
from utilities.request_check import request_check

get_profile_fields = ['user']


class Profile(Resource):

    def get(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        args = request.args
        if request_check(get_profile_fields, args):
            return request_check(get_profile_fields, args)

        username = encode_token(request.headers['token'])
        validate_user = db.user.find_one({'user': username})
        if not validate_user:
            return {'error': True, 'message': 'token is wrong'}
        # Todo: get length of array instead of fetch all of the array
        validate_target_user = db.user.find_one({'user': args['user']},
                                                {'_id': 0, 'user': 1, 'name': 3, 'last_name': 4, 'avatar': 5,
                                                 'private_mode': 7,
                                                 'posts': 8,
                                                 'followers': 9, 'following': 10})
        if not validate_target_user:
            return {'error': True, 'message': 'target user is not valid'}

        return {'error': False,
                'personal_data': {'user': validate_target_user['user'], 'name': validate_target_user['name'],
                                  'last_name': validate_target_user['last_name'],
                                  'avatar': validate_target_user['avatar'],
                                  'private_mode': validate_target_user['private_mode'],
                                  'posts': len(validate_target_user['posts']),
                                  'followers': len(validate_target_user['followers']),
                                  'following': len(validate_target_user['following']),
                                  'status': check_two_user(username, args['user'])}}
