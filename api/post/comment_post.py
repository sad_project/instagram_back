import random

from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token
from utilities.access.user import access_user
from utilities.get_db_variables import get_db_variables
from utilities.hash_by_time import hash_by_time
from utilities.request_check import check_post_request, request_check

expected_request = ['post_id', 'user', 'comment_text']
get_post_comments_fields = ['post_id', 'user']


class CommentPost(Resource):
    def get(self):
        args = request.args

        if request_check(get_post_comments_fields, args):
            return request_check(get_post_comments_fields, args)

        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        current_user = encode_token(request.headers['token'])
        target_user = args['user']

        if not current_user:
            return {'error': True, 'message': "send token in request header"}

        user_validation, target_user_validation, target_post = get_db_variables(current_user, target_user, args)

        if not user_validation:
            return {'error': True, 'message': "The current user doesn't exist!"}

        if not target_user_validation:
            return {'error': True, 'message': "The target user doesn't exist!"}

        if not access_user(current_user, target_user):
            return {'error': True, 'message': 'you do not have permission'}

        if not target_post:
            return {'error': True, 'message': "The target post doesn't exist"}

        # fetch comments array from target_post
        comments_array = target_post['comments']

        # fetch user and avatar of users comments under that post
        comment_users = list(db.user.find({'_id': {'$in': list(map(lambda comment: comment['_id'], comments_array))}},
                                          {'_id': 1, 'user': 2, 'avatar': 3}))

        # remove user document id  from comments and insert username and user avatar to comment
        for comment in comments_array:
            for user in comment_users:
                if comment['_id'] == user['_id']:
                    del comment['_id']
                    comment['user'] = user['user']
                    comment['avatar'] = user['avatar']

        return {'error': False, 'comments': list(comments_array)}

    def post(self):
        form = request.form
        if check_post_request(expected_request, form):
            return check_post_request(expected_request, form)
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        current_user = encode_token(request.headers['token'])
        target_user = form['user']

        if not current_user:
            return {'error': True, 'message': "send token in request header"}

        user_validation, target_user_validation, target_post = get_db_variables(current_user, target_user, form)

        if not user_validation:
            return {'error': True, 'message': "The current user doesn't exist!"}

        if not target_user_validation:
            return {'error': True, 'message': "The target user doesn't exist!"}

        if not target_post:
            return {'error': True, 'message': "The target post doesn't exist"}

        if access_user(current_user, target_user):
            comment_add = {
                "comment_id": hash_by_time() + str(random.randint(10000000, 99999999)),
                # save user document id
                "_id": user_validation['_id'],
                "comment_text": form['comment_text'],
                "likes": [],
            }

            db.user.update(
                {'user': target_user, "posts.post_id": form['post_id']},
                {'$push': {"posts.$.comments": comment_add}}
            )
            return {'error': False, 'message': "comment to target_user has been sent"}
        return {'error': True, 'message': 'you do not have permission'}
