from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token
from utilities.get_db_variables import get_db_variables
from utilities.request_check import check_post_request, request_check

expected_request = ['post_id', 'user']

'''
if user has liked the post dislike
and if has'nt liked like the post
'''


class LikePost(Resource):

    def get(self):
        args = request.args
        if request_check(expected_request, args):
            return request_check(expected_request, args)

        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        current_user = encode_token(request.headers['token'])
        target_user = args['user']

        if not current_user:
            return {'error': True, 'message': "send token in request header"}

        user_validation, target_user_validation, target_post = get_db_variables(current_user, target_user, args)

        if not user_validation:
            return {'error': True, 'message': "The current user doesn't exist!"}

        if not target_user_validation:
            return {'error': True, 'message': "The target user doesn't exist!"}

        if not target_post:
            return {'error': True, 'message': "The target post doesn't exist"}

        likes_id_array = target_post['likes']

        user_likes = db.user.find({'_id': {'$in': list(map(lambda likes_id: likes_id['_id'], likes_id_array))}},
                                  {'_id': 0, 'user': 1, 'avatar': 2})
        return {'error': False, 'likes': list(user_likes)}

    def post(self):
        form = request.form
        if check_post_request(expected_request, form):
            return check_post_request(expected_request, form)

        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        current_user = encode_token(request.headers['token'])
        target_user = form['user']

        if not current_user:
            return {'error': True, 'message': "send token in request header"}

        user_validation, target_user_validation, target_post = get_db_variables(current_user, target_user, form)

        if not user_validation:
            return {'error': True, 'message': "The current user doesn't exist!"}

        if not target_user_validation:
            return {'error': True, 'message': "The target user doesn't exist!"}

        if not target_post:
            return {'error': True, 'message': "The target post doesn't exist"}
        # check if object of user id exists in likes array pop it from likes array
        if {'_id': user_validation['_id']} in target_post['likes']:
            db.user.update(
                {'user': target_user, "posts.post_id": form['post_id']},
                # pull user document id from likes array
                {'$pull': {"posts.$.likes": {'_id': user_validation['_id']}}}
            )
            return_message = "you disliked this post"

        else:
            db.user.update(
                {'user': target_user, "posts.post_id": form['post_id']},
                # push user document id from likes array
                {'$push': {"posts.$.likes": {'_id': user_validation['_id']}}}
            )
            return_message = "you liked this post"

        return {'error': False, 'message': return_message}
