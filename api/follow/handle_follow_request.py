from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token
from utilities.request_check import check_post_request

accept_follow_requests_fields = ['user', 'accept_ignore']

'''
Accept function gets token and user token
is the username of the acceptor and user
is the username of the requester
'''


class HandleFollowRequest(Resource):

    def post(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}

        if check_post_request(accept_follow_requests_fields, request.form):
            return check_post_request(accept_follow_requests_fields, request.form)

        current_user = encode_token(request.headers['token'])
        target_user = request.form['user']

        user_validation = db.user.find_one({'user': current_user})
        target_user_validation = db.user.find_one({'user': target_user})

        if user_validation is None:
            return {'error': True, 'message': 'token is wrong'}

        if target_user_validation is None:
            return {'error': True, 'message': 'target user is not valid'}

        validate_follow_request = db.user.find_one(
            {'user': current_user, 'follow_requests': {'$elemMatch': {'_id': target_user_validation['_id']}}})

        if validate_follow_request is None:
            return {'error': True, 'message': 'user is not in follow request list'}

        # updating connections
        # accept follow request
        if request.form['accept_ignore'] == 'accept':
            db.user.update({'user': current_user},
                           {'$pull': {'follow_requests': {'_id': target_user_validation['_id']}},
                            '$push': {'followers': {'_id': target_user_validation['_id']}}})

            db.user.update({'user': target_user}, {'$push': {'following': {'_id': user_validation['_id']}}})

        # ignore follow request

        elif request.form['accept_ignore'] == 'ignore':
            db.user.update({'user': current_user},
                           {'$pull': {'follow_requests': {'_id': target_user_validation['_id']}}})
            return {'error': False, 'message': "follow request ignored successfully"}
        else:
            return {'error': True, 'message': "accept_ignore key should be 'ignore' or 'accept'"}

        return {'error': False, 'message': "follow request accepted successfully"}
