from flask import request
from flask_restful import Resource

from mongo_config import db
from security.token import encode_token
from utilities.access.user import access_user

get_post_list_fields = ['user']


class PostList(Resource):

    def get(self):
        if 'token' not in request.headers:
            return {'error': True, 'message': 'append token in request header'}
        args = request.args

        # target_user = ''
        if 'user' in args:
            target_user = args['user']
        else:
            target_user = encode_token(request.headers['token'])
        current_user = encode_token(request.headers['token'])

        validation = db.user.find_one({'user': current_user})
        if not validation:
            return {'error': True, 'message': 'token is wrong'}

        target_user_validation = db.user.find_one({'user': target_user})

        if not target_user_validation:
            return {'error': True, 'message': 'target user is not valid'}

        if not access_user(current_user, target_user):
            return {'error': True, 'message': 'current user does not have permission', 'access': False}

        target_user_posts = db.user.find_one({'user': target_user}, {'posts': 1})['posts']

        for post in target_user_posts:

            # check if current user likes that specific post
            post['liked'] = True if ({'_id': validation['_id']} in post['likes']) else False

            # just return number of likes and comments
            post['likes'] = len(post['likes'])
            post['comments'] = len(post['comments'])
            # set image url for each image in document
            for image in post['images']:
                target_user_posts[target_user_posts.index(post)]['images'][
                    target_user_posts[target_user_posts.index(post)]['images'].index(
                        image)] = '/post/files?user=' + target_user + '&post_id=' + post['post_id'] + '&file=' + image
        return {'error': False, 'posts': target_user_posts, 'access': True}
