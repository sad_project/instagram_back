import os

from flask import request
from flask_restful import Resource

from files_directory import file_directory
from mongo_config import db
from security.token import get_token
from utilities.hash_by_time import hash_by_time
from utilities.request_check import request_check, check_post_request

expected_request = ['user', 'password', 'name', 'last_name', 'private_mode']


class SignUp(Resource):

    def post(self):
        if check_post_request(expected_request, request.form):
            return request_check(expected_request, request.form)

        validate_username = db.user.find_one({'user': request.form['user']})

        if validate_username:
            return {'error': True, 'message': 'this username is already in use'}

        id = db.user.insert(
            {'user': request.form['user'], 'password': request.form['password'], 'name': request.form['name'],
             'last_name': request.form['last_name'],
             'avatar': '/user/avatar?user=' + request.form['user'] + '_' + hash_by_time(),
             'private_mode': bool(request.form['private_mode']), 'posts': [], 'followers': [], 'following': [],
             'follow_requests': [], 'chats': []})
        # create path in files directory for user posts
        user_path = file_directory + '/' + str(id)
        os.makedirs(user_path)

        return {'error': False, 'token': get_token(request.form['user']),
                'avatar': '/user/avatar?user=' + request.form['user'] + '_' + hash_by_time()}
