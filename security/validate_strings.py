# validate passwords length to be
# more than 8 and include alphabet and numbers

def validate_password(password):
    if len(password) > 7:
        has_digits = any(char.isdigit() for char in password)
        has_alphabet = not password.isdigit()

        if has_digits & has_alphabet:
            return True

    return False

# validate emails to be like xx@xx.xx

def validate_email(email):
    if email.count('@') == 1:
        first_part,second_part = email.split('@')
        if first_part:
            if second_part:
                if second_part.count('.') == 1:
                    d_first,d_second = second_part.split('.')
                    if d_first and d_second:
                        return True
    return False
