from flask import request
from flask_restful import Resource

from mongo_config import db
from utilities.request_check import check_post_request
from utilities.return_users_id import return_users_id, return_user_name
from security.token import encode_token

expected_request = ['token']


class GetChatList(Resource):
    def post(self):
        if check_post_request(expected_request, request.form):
            return check_post_request(expected_request, request.form)

        user_name = encode_token(request.form['token'])
        user_document, user_id = return_users_id([user_name])
        if not user_document:
            return {'error': True, 'message': "token error error", 'chat_list': []}
        chats = user_document[0]['chats']
        chat_list = []

        for i in chats:
            x = db.chat.find_one({'_id': i['chat_id']})
            users = return_user_name([x['first_user_id'], x['second_user_id']])
            chat_list.append({"chat_id": i['chat_id'], "user1": users[0], "user2": users[1]})

        return {'error': False, 'message': 'message sent successfully', 'chat_list': chat_list}
